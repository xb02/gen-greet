#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import requests
import datetime
import argparse

from requests.packages.urllib3.exceptions import InsecureRequestWarning
from io import BytesIO

from PIL import Image, ImageDraw, ImageFont

RESIZE_FACTOR = 90

gFontPath = {"en": "FreeMonoBold.ttf", "cn": "ukai.ttc"}
greet = {
  "gg" : {
    "en": "  Greetings !  ",
    "cn": u'问候！',
    "qt": "nature",
    "url": "https://source.unsplash.com/1600x900/?"
  },
  "gm" : {
    "en": " Good Morning! ",
    "cn": u'早安!',
    "qt": "sunrise,nature",
    "url": "https://source.unsplash.com/1600x900/?"
  },
  "ga" : {
    "en": "Good Afternoon!",
    "cn": u'下午好!',
    "qt": "afternoon,nature",
    "url": "https://source.unsplash.com/1600x900/?"
  },
  "ge" : {
    "en": " Good Evening! ",
    "cn": u'晚上好',
    "qt": "evening,nature",
    "url": "https://source.unsplash.com/1600x900/?"
  },
  "gn" : {
    "en": "  Good Night!  ",
    "cn": u'晚安!',
    "qt": "moon,nature",
    "url": "https://source.unsplash.com/1600x900/?"
  }
}

class dailyImage:
  def __init__(self, url_file='', dbg=False):
    self.ow = self.oh = 0 # original width, height
    self.rw = self.rh = 0 # resize   width, height
    self.loc = None
    self.org_img = None
    self.rsz_img = None
    self.draw = None
    self.rsz_fac = RESIZE_FACTOR
    try:
      self.loc = f"url: {url_file}"
      self.dwnld_img_from_url(url_file, dbg)
    except: #possibly invalid url, check for file
      try:
        self.loc = f"file: {url_file}"
        self.read_img_from_file(url_file)
      except:
        print("not valid url or file")
        exit(1)
    # end of init

  def __repr__(self):
    outstr = f"loc: {self.loc}, wid: {self.ow}, ht: {self.oh}"
    return outstr

  def dwnld_img_from_url(self, url, dbg=False):
    if dbg: print(f"dwnld_img_from_url: url: {url}")
    requests.packages.urllib3.disable_warnings()
    rsp = requests.get(url, verify=False)
    self.org_img = Image.open(BytesIO(rsp.content))
    self.ow, self.oh = self.org_img.size
    return

  def read_img_from_file(self, ifile, dbg=False):
    if dbg: print(f"read_img_from_file: file: {ifile}")
    self.org_img = Image.open(ifile)
    self.ow, self.oh = self.org_img.size
    return

  def resize_image(self, dbg=False):
    self.rw = self.rsz_fac * 16
    self.rh = self.rsz_fac * 9
    self.rsz_img = self.org_img.resize((self.rw, self.rh), Image.ANTIALIAS)
    self.rw, self.rh = self.rsz_img.size
    self.draw = ImageDraw.Draw(self.rsz_img)
    if dbg: print(f"resize: wid:{self.rw}, ht:{self.rh}")
    return

  def text_image(self, fg, bg, txt, fpath, size, offset=(0, 0), dbg=False):
    if dbg: print(f"fpath: {fpath}")
    font = ImageFont.truetype(fpath, size)
    txt_w, txt_h = self.draw.textsize(txt, font)
    txt_img = Image.new('RGB', (txt_w+10, txt_h+10), color=bg)
    txt_draw = ImageDraw.Draw(txt_img)
    txt_draw.text((offset[0]+5, offset[1]+0), txt, font=font, fill=fg)
    return (txt_img, (txt_w, txt_h))

  def add_watermark(self, txt, isize=(0, 0), dbg=False):
    bg = (255, 255, 0)
    fg = (255, 000, 0)
    sz = 20
    img, size = self.text_image(fg, bg, txt, gFontPath["en"], sz, dbg=dbg)
    if dbg: print(f"watermark: {size[0]}x{size[1]} ({txt}): ")
    x = int(isize[0])
    y = int(self.rh - size[1] - 5)
    self.rsz_img.paste(img, (x, y))
    return size

  def add_timestamp(self, txt, isize=(0, 0), dbg=False):
    fg = (0, 255, 255)
    bg = (0, 000, 000)
    sz = 20
    if not txt:
      t = datetime.datetime.now()
      txt = t.strftime(' %m/%d/%Y')
    img, size = self.text_image(fg, bg, txt, gFontPath["en"], sz, offset=(0, 3), dbg=dbg)
    if dbg: print(f"timestamp: {size[0]}x{size[1]} ({txt}): ")
    x = int(isize[0] + 5)
    y = int(self.rh - size[1] - 5)
    self.rsz_img.paste(img, (x, y))
    return size

  def add_message(self, txt, isize=(0, 0), dbg=False):
    #fg = (255, 97, 3)
    fg = (255,  0, 0)
    #bg = (238, 197, 145)
    bg = (  0,   0,   0, 0)
    sz = 30
    img, size = self.text_image(fg, bg, txt, gFontPath["en"], sz, offset=(0, 0), dbg=dbg)
    if dbg: print(f"message: {size[0]}x{size[1]} ({txt}): ")
    x = int((self.rw - size[0])/2 + 5)
    y = int(self.rh - size[1] - isize[1] - 5)
    if dbg: print(f"x: {x}, y: {y}")
    self.rsz_img.paste(img, (x, y))
    return size

  def add_en_greeting(self, txt, isize=(0, 0), dbg=False):
    fg = (128, 255, 000)
    #bg = (153, 153, 153)
    bg = (  0,   0,   0, 0)
    sz = 40
    img, size = self.text_image(fg, bg, txt, gFontPath["en"], sz, offset=(0, 0), dbg=dbg)
    if dbg: print(f"en_greet: {size[0]}x{size[1]} ({txt}): ")
    x = isize[0]
    y = isize[1]
    self.rsz_img.paste(img, (x, y))
    return size

  def add_cn_greeting(self, txt, isize=(0, 0), dbg=False):
    #bg = (255, 228, 196)
    bg = (  0,   0,   0, 0)
    fg = (255, 000, 255)
    sz = 50
    img, size = self.text_image(fg, bg, txt, gFontPath["cn"], sz, offset=(0, 0), dbg=dbg)
    if dbg: print(f"cn_greet: {size[0]}x{size[1]} ({'chinese'})")
    x = self.rw - size[0] - 5
    y = self.rh - size[1] - 5
    self.rsz_img.paste(img, (x, y))
    return size

  def save_image_to_output(self, ofile, otype, greettype, dbg=False):
    t = datetime.datetime.now()
    if ofile:
      outfile = f"{ofile}-{greettype}.{otype}"
    else:
      outfile = t.strftime(f"%m%d-{greettype}.gif")
    if dbg: print(f"save_image_to_output: outfile:{outfile}")
    self.rsz_img.save(outfile)
    return

def get_url_file(greet_args, greet_qry, greet_stored):
  if greet_args.startswith("http"):
    if greet_qry:
      return greet_args + greet_qry
    elif greet_args.endswith("/?"):
      return greet_args + greet_stored
  return greet_args
 
def parse_args(args):
  parser = argparse.ArgumentParser()
  parser.add_argument("-d", "--debug", action="store_true",
                      dest="debug", help="enable debugging")
  parser.add_argument("-v", "--verbose", action="store_true",
                      dest="verbose", help="print verbose debugging")
  parser.add_argument("-o", "--outfile", action="store",
                      dest="ofile",
                      help="local file prefix, default date based")
  parser.add_argument("-t", "--type", action="store", default="gif",
                      dest="type",
                      help="type, gif, png, jpg, jpeg (default: gif)")
  parser.add_argument("-n", "--name", action="store", dest="name",
                      help="name for water marking")
  parser.add_argument("-he", "--hide-english", action="store_true", dest="he",
                      help="print english message")
  parser.add_argument("-hc", "--hide-chinese", action="store_true", dest="hc",
                      help="print chinese message")
  parser.add_argument("-m", "--mesg", action="store", dest="mesg",
                      help="additional message to add")
  parser.add_argument("-q", "--qry", action="store", dest="qry",
                      help="search query for image")
  parser.add_argument("-dt", action="store", dest="dt",
                      help="explict date in the format mm/dd/YYYY as string")

  greet_group = parser.add_mutually_exclusive_group()
  greet_group.add_argument("-gm", type=str, nargs='?',
                           const=greet['gm']['url'],
                           help="good morning")
  greet_group.add_argument("-ga", type=str, nargs='?',
                           const=greet['ga']['url'],
                           help="good afternoon")
  greet_group.add_argument("-ge", type=str, nargs='?',
                           const=greet['ge']['url'],
                           help="good evening")
  greet_group.add_argument("-gn", type=str, nargs='?',
                           const=greet['gn']['url'],
                           help="good night")
  greet_group.add_argument("-gg", type=str, nargs='?',
                           const=greet['gg']['url'],
                           help="generic greeting")
  greet_group.add_argument("-gx", type=str, nargs='?',
                           help="all the above")

  return parser.parse_args(args)

def main(args=None):
  args = parse_args(args)

  if args.verbose:
    print(args)

  glist = list();
  dbg = args.debug
  if args.gm:
    greet['gm']['url'] = get_url_file(args.gm, args.qry, greet['gm']['qt'])
    glist.append('gm');
  elif args.ga:
    greet['ga']['url'] = get_url_file(args.ga, args.qry, greet['ga']['qt'])
    glist.append('ga');
  elif args.ge:
    greet['ge']['url'] = get_url_file(args.ge, args.qry, greet['ge']['qt'])
    glist.append('ge');
  elif args.gn:
    greet['gn']['url'] = get_url_file(args.gn, args.qry, greet['gn']['qt'])
    glist.append('gn');
  elif args.gg:
    greet['gg']['url'] = get_url_file(args.gg, args.qry, greet['gg']['qt'])
    glist.append('gg');
  elif args.gx:
    glist.extend(['gm', 'ga', 'ge', 'gn', 'gg'])
    for tp in greet.keys():
      greet[tp]['url'] = get_url_file(args.gx, args.qry, greet['gx']['qt'])
  else:
    glist.extend(['gm', 'ga', 'ge', 'gn', 'gg'])

  if dbg:
    if args.he: print("No english message will be printed")
    if args.hc: print("No chinese message will be printed")
    print(f"greet list: {glist}")
    for gtype in glist:
      print(f"Greeting:  {gtype}")
      print(f"Greethash: {greet[gtype]}")
      print(f"English :  {greet[gtype]['en']}")
      print(f"URL:       {greet[gtype]['url']}")

  for gtype in glist:
    dI = dailyImage(url_file=greet[gtype]['url'], dbg=dbg)
    if dbg: print(dI)

    dI.resize_image(dbg=dbg)

    name = args.name if args.name else 'b a l a'
    size = dI.add_watermark(name, dbg=dbg)
    size = dI.add_timestamp(args.dt, isize=size, dbg=dbg)

    if args.mesg:
      size = dI.add_message(args.mesg, isize=size, dbg=dbg)

    if not args.he:
      size = dI.add_en_greeting(greet[gtype]['en'], dbg=dbg)
    if not args.hc:
      size = dI.add_cn_greeting(greet[gtype]['cn'], dbg=dbg)
    dI.save_image_to_output(args.ofile, args.type, gtype, dbg=dbg)
    # end of for

  return


if __name__ == "__main__":
  main()

