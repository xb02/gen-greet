#! /usr/bin/env python3

from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient.http import MediaFileUpload
import json
import datetime


try:
  import argparse
  flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
  flags = None

SCOPE = 'https://www.googleapis.com/auth/drive'
AUTH_JSON = '.auth.json'
CRED_JSON = '.cred.json'
APPLICATION_NAME = 'GM02'

def get_credentials():
  credential_path = AUTH_JSON
  store = Storage(credential_path)
  credentials = store.get()
  if not credentials or credentials.invalid:
    flow = client.flow_from_clientsecrets(CRED_JSON, SCOPE)
    flow.user_agent = APPLICATION_NAME
    if flags:
      credentials = tools.run_flow(flow, store, flags)
    else: # Needed only for compatibility with Python 2.6
      credentials = tools.run(flow, store)
  return credentials

def main():
  t = datetime.datetime.now()
  dailygreetgifs = [
    # GIF folder file name, GIF folder file id, GM folder file name,  GM folder file id
    [ t.strftime('%m%d-gm.gif'),  '', 'GoodMorning.gif',    '' ],
    [ t.strftime('%m%d-ga.gif'),  '', 'GoodAfternoon.gif',  '' ],
    [ t.strftime('%m%d-ge.gif'),  '', 'GoodEvening.gif',    '' ],
    [ t.strftime('%m%d-gn.gif'),  '', 'GoodNight.gif',      '' ]
  ]

  credentials = get_credentials()
  http = credentials.authorize(httplib2.Http())
  service = discovery.build('drive', 'v3', http=http)
  
  # query for GM and GIFs folder and get their id's
  # id's are needed to upload the files to right folder
  api_fields = 'nextPageToken, files(id, name)'
  api_query  = "mimeType='application/vnd.google-apps.folder' "
  api_query += "and trashed = false and "
  api_query += "(name = '{}' or name = '{}')".format('GM', 'GIFs')
  response = service.files().list(q=api_query, spaces='drive', 
                            fields=api_fields, pageToken=None).execute()

  gifs_folder_id = gm_folder_id = ''
  for folders in response.get('files', []):
    folder_name    = folders.get('name')
    folder_id      = folders.get('id')

    print("Found folder: {} (id: {})".format(folder_name, folder_id))
    if folder_name == 'GIFs': gifs_folder_id = folder_id
    if folder_name == 'GM':   gm_folder_id   = folder_id
  
  if not gifs_folder_id or not gm_folder_id:
    print("GM or GIFs folder not found")
    exit(1)

  # now get the GIF files and their id's
  api_fields = 'nextPageToken, files(id, name)'
  api_query  = "mimeType='image/gif' and trashed = false and "
  dg_len = len(dailygreetgifs)
  api_query += "("
  for l_ax in range(dg_len):
    api_query += " name = '{}' or name = '{}'".format(dailygreetgifs[l_ax][0], dailygreetgifs[l_ax][2])
    if l_ax != (dg_len - 1):
      api_query += " or "
  api_query += ")"
  response = service.files().list(q=api_query, spaces='drive', 
                            fields=api_fields, pageToken=None).execute()

  for prop_file in response.get('files', []):
    prop_filename  = prop_file.get('name')
    prop_fileid    = prop_file.get('id')
    print("file: {} (id: {})".format(prop_filename, prop_fileid))
    for ax in range(len(dailygreetgifs)):
      if dailygreetgifs[ax][0] == prop_filename:
        dailygreetgifs[ax][1] = prop_fileid
      if dailygreetgifs[ax][2] == prop_filename:
        dailygreetgifs[ax][3] = prop_fileid

  for ax in range(len(dailygreetgifs)):
    df_name = dailygreetgifs[ax][0]
    df_id   = dailygreetgifs[ax][1]
    gf_name = dailygreetgifs[ax][2]
    gf_id   = dailygreetgifs[ax][3]

    # check if we found file-id, this is for GIFs folder, see else section
    if df_id:
      print("Update existing file for : {}".format(df_name))
      file_metadata = {'name': df_name, 'originalFilename': df_name }
      media = MediaFileUpload(df_name, mimetype='image/gif')
      fileu = service.files().update(fileId=df_id, body=file_metadata, media_body=media).execute()
    else:
      print("New file creation for : {}".format(df_name))
      file_metadata = {'name': df_name, 'parents': [gifs_folder_id] }
      media = MediaFileUpload(df_name, mimetype='image/gif')
      fileu = service.files().create(body=file_metadata, media_body=media, fields='id').execute()

    # check if we found file-id, this is for GM folder, see else section
    if gf_id:
      print("Update existing file for : {}".format(gf_name))
      file_metadata = {'name': gf_name, 'originalFilename': df_name }
      media = MediaFileUpload(df_name, mimetype='image/gif')
      fileu = service.files().update(fileId=gf_id, body=file_metadata, media_body=media).execute()
    else:
      print("New file creation for : {}".format(gf_name))
      file_metadata = {'name': gf_name, 'parents': [gm_folder_id] }
      media = MediaFileUpload(df_name, mimetype='image/gif')
      fileu = service.files().create(body=file_metadata, media_body=media, fields='id').execute()

    #end for

if __name__ == '__main__':
    main()
