
## Public URL
http://xb02.gitlab.io/gen-greet

## Good Morning greetings 
http://xb02.gitlab.io/gen-greet/index.html

http://xb02.gitlab.io/gen-greet/daily.html

http://bit.ly/balagm1


## Other files 
http://xb02.gitlab.io/gen-greet/day0.html

http://xb02.gitlab.io/gen-greet/pre1.html

http://xb02.gitlab.io/gen-greet/pre2.html

http://xb02.gitlab.io/gen-greet/pre3.html

http://xb02.gitlab.io/gen-greet/nex1.html

http://xb02.gitlab.io/gen-greet/nex2.html

http://xb02.gitlab.io/gen-greet/nex3.html

http://bit.ly/balaga1

http://bit.ly/balage1

http://bit.ly/balagn1

----
## Command line instructions

### Git config setup for this repo

```
git config user.name "Balaji Venkataraman -  xb02"
git config user.email "xbala.ji.v+gitlab@gmail.com"
```

### Create a new repository

```
git clone git@gitlab.com:xb02/gen-greet
cd gen-greet
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Existing folder

```
cd existing_folder
git init
git remote add origin git@gitlab.com:xb02/gen-greet.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:xb02/gen-greet.git
git push -u origin --all
git push -u origin --tags
```

#### 
```
pip3 install datetime requests argparse Pillow              # gen-greet.py
pip3 install google-api-python-client oauth2client          # gdrive-upload.py
```
